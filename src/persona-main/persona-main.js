import { LitElement, html } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';


class PersonaMain extends LitElement {
    
    static get properties() {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }
    
    constructor() {
        super();

        this.people = [
            {
                name: "Ellen Ripley",
                yearsInCompany: 25,
                profile: "Lorem ipsum dolor sit amet.",
                photo: {
                    src: "./img/persona1.jpg",
                    alt: "Ellen Ripley"
                }
            }, {
                name: "Bruce Banner",
                yearsInCompany: 5,
                profile: "Lorem ipsum.",
                photo: {
                    src: "./img/persona2.jpg",
                    alt: "Bruce Banner"
                }
            }, {
                name: "Marylin Monroe",
                yearsInCompany: 8,
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
                photo: {
                    src: "./img/persona3.jpg",
                    alt: "Marylin Monroe"
                }
            }, {
                name: "George Clooney",
                yearsInCompany: 12,
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
                photo: {
                    src: "./img/persona2.jpg",
                    alt: "George Clooney"
                }
            }, {
                name: "Kim Basinger",
                yearsInCompany: 3,
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                photo: {
                    src: "./img/persona3.jpg",
                    alt: "Kim Basinger"
                }
            }
        ];

        this.showPersonForm = false;
    }
    
    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.map(
                        person => html`
                        <persona-ficha-listado 
                            fname="${person.name}" 
                            yearsInCompany="${person.yearsInCompany}"
                            profile="${person.profile}"
                            .photo="${person.photo}"
                            @delete-person="${this.deletePerson}"
                            @info-person="${this.infoPerson}"
                        >
                        </persona-ficha-listado>`
                    )}
                </div>
            </div>
            <div class="row">
                <persona-form 
                    @persona-form-close="${this.personFormClose}"
                    @persona-form-store="${this.personFormStore}"
                    class="d-none border rounded border-primary" id="personaForm"></persona-form>
            </div>
        `;
    }

    updated(changedProperties) {
        console.log("dentro de updated");

        if (changedProperties.has("showPersonForm")) {
            console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");

            if (this.showPersonForm === true) {
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }

        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en persona-main");

            this.dispatchEvent(new CustomEvent("updated-people", {
                    "detail": {
                        people: this.people
                    }
                }
            ));
        }
    }

    deletePerson(e) {
        console.log("Dentro de deletePerson en persona-main");
        console.log("Borrando la persona: " + e.detail.name);

        this.people = this.people.filter(
            person => person.name != e.detail.name
        );
    }

    infoPerson(e) {
        console.log("Dentro de infoPerson en persona-main");
        console.log("Se ha pedido mas informacion de la persona: " + e.detail.name);

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name

        );

        console.log(chosenPerson);

        let personToShow = {};
        personToShow.name = chosenPerson[0].name;
        personToShow.profile = chosenPerson[0].profile;
        personToShow.yearsInCompany = chosenPerson[0].yearsInCompany;

        this.shadowRoot.getElementById("personaForm").person = personToShow;
        this.shadowRoot.getElementById("personaForm").editingPerson = true;
        this.showPersonForm = true;
    }

    showPersonFormData() {
        console.log("Dentro de showPersonForm");
        console.log("Mostrando el formulario de nueva persona");

        this.shadowRoot.getElementById("personaForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none")
    }

    showPersonList() {
        console.log("Dentro de showPersonList");
        console.log("Mostrando el listado de personas");

        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personaForm").classList.add("d-none")
    }    

    personFormClose() {
        console.log("Dentro de personFormClose en persona-main");
        console.log("Se va a cerrar el formulario de la persona...");
        
        this.showPersonForm = false;
    }

    personFormStore(e) {
        console.log("Dentro de personFormStore en persona-main");
        console.log("Se va a almacentar una persona...");

        console.log("La propiedad name en person vale " + e.detail.person.name);
        console.log("La propiedad profile en person vale " + e.detail.person.profile);
        console.log("La propiedad yearsInCompany en person vale " + e.detail.person.yearsInCompany);
        console.log("La propiedad editingPerson vale " + e.detail.editingPerson);


        if (e.detail.editingPerson != true ) {
            console.log("Se va a almacenar una persona nueva");
            
            //Comentada la forma antigua de actualizar
            //this.people.push(e.detail.person);

            //Hacemos esto para asegurarnos que Litelement, detecte que la propiedad person cambia en el updated            
            this.people = [...this.people, e.detail.person]
            
        } else {
            console.log("Se va a actualizar la persona: " + e.detail.person.name);
            
            //Comentada la forma antigua de actualizar
            // let indexOfPerson = this.people.findIndex(
            //     person => person.name === e.detail.person.name
            // );

            // if (indexOfPerson >= 0) {
            //     console.log("Persona encontrada en index: " + indexOfPerson);
            //     this.people[indexOfPerson].name = e.detail.person.name;
            //     this.people[indexOfPerson].profile = e.detail.person.profile;
            //     this.people[indexOfPerson].yearsInCompany = e.detail.person.yearsInCompany;
            // }

            //Hacemos esto para asegurarnos que Litelement, detecte que la propiedad person cambia en el updated
            this.people = this.people.map(
                person => person.name === e.detail.person.name
                            ? person = e.detail.person : person
            );

            // //Hacemos esto para asegurarnos que Litelement, detecte que la propiedad person cambia en el updated
            // this.people = this.people.map(
            //     person => if (person.name === e.detail.person.name) {
            //                     person.name = e.detail.person.name;
            //                     person.profile = e.detail.person.profile;
            //                     person.yearsInCompany = e.detail.person.yearsInCompan;
            //                 } else {
            //                     person = person;
            //                 }
            // );
        }

        console.log("Persona almacenada en el listado");

        this.showPersonForm = false;
    }
}

customElements.define('persona-main', PersonaMain)
import { LitElement, html } from 'lit-element';

class FichaPersona extends LitElement {
    
    static get properties() {
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String},
            photo: {type: Object}
        };
    }
    
    constructor() {
        super();

        this.name = "Prueba Nombre";
        this.yearsInCompany = "15";
        this.updatePersonInfo();

    }
    
    render() {
        return html`
            <div>
                <label>Nombre Completo</label>
                <input type="text" id="fname" value="${this.name}" @input="${this.updateName}"></input>
                <br />
                <label>Años en la empresa</label>
                <input type="text" name="yearsInCompany" value="${this.yearsInCompany}" @change="${this.updateYearsInCompany}"></input>
                <br />
                <input type="text" value="${this.personInfo}" disabled></input>
                <br />
            </div>
        `;
    }

    updated(changedPropierties){
        changedPropierties.forEach((oldvalue, propName) => {
            console.log("Propiedad " + propName + " cambia valor, anterior era " + oldvalue);
        });

        if (changedPropierties.has("name")) {
            console.log("Propiedad name cambia valor anterior era " + changedPropierties.get("name")
                + " nuevo valor es " + this.name);

        }

        if (changedPropierties.has("yearsInCompany")) {
            console.log("Propiedad yearsInCompany cambia valor anterior era " + changedPropierties.get("yearsInCompany")
                + " nuevo valor es " + this.yearsInCompany);
            this.updatePersonInfo();
        }
    }

    updateName(event){
        console.log("Dentro de updateYearsInCompany");
        this.name = event.target.value;
    }

    updateYearsInCompany(event){
        console.log("Dentro de updateName");
        this.yearsInCompany = event.target.value;
    }

    updatePersonInfo(event){
        console.log("Dentro de updatePersonInfo");
        console.log("yearsInCompany vale " + this.yearsInCompany);

        if (this.yearsInCompany >= 7) {
            this.personInfo = "lead";
        } else if (this.yearsInCompany >= 5) {
            this.personInfo = "senior";
        } else if (this.yearsInCompany >= 3) {
            this.personInfo = "team";
        } else {
            this.personInfo = "junior";
        }

    }

        
    
}

customElements.define('ficha-persona', FichaPersona)